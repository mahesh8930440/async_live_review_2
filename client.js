const fetch = require("node-fetch");

async function fetchValuesConcurrently(length){
  const duplicateData=[];
  
  for(let row=0;row<length;row++){
    const columnData=[];
    
    for(let column=0;column<length;column+=2){
      let firstResponse=await fetch(`http://localhost:3001/value?rowIndex=${row}&colIndex=${column}`);
      let secondResponse=await fetch(`http://localhost:3001/value?rowIndex=${row}&colIndex=${column+1}`);
      let [firstValue,secondValue] =await Promise.all([firstResponse.json(),secondResponse.json()])
      columnData.push(firstValue.value);
      columnData.push(secondValue.value);
    }
    duplicateData.push(columnData);
  }
  console.log(duplicateData);
  
} 
async function main() {
  const response = await fetch("http://localhost:3001/initialize");
  const data = await response.json();
  fetchValuesConcurrently(data.size);
}
main();
